<div class="container">
  <div class="row">
  <div class="card">
  <div class="card-header">
    <h3 class="card-title">報帳謄打系統</h3>
  </div>
    <!--header-->
    <!--body-->

  <div class="card-body">
          <form name="myform">
          <div class="form-group">
            <label for="exampleFormControlInput1">來源網址:https://www.youtube.com</label>
            <textarea class="form-control" id="urLink" rows="3"></textarea>
          </div>


          <div class="form-group">
            <label for="">行銷活動來源</label>
            <input type="text" class="form-control" id="utmSource">
          </div>

          <div class="form-group">
            <label for="exampleFormControlSelect1">行銷活動媒介</label>
            <select class="form-control" id="utmMedium">
                <option>post</option>
                <option>ad</option>
                <option>edm</option>
                <option>line</option>
                <option>blogger</option>
            </select>
          </div>
            
          <div class="form-group">
            <label for="exampleFormControlInput1">行銷活動名稱</label>
            <input type="text" class="form-control" id="utmCampaign" placeholder="2019-05-summerSale">
          </div>
          
            <div class="form-row">
            <div class="col-md-3 mb-3">
              <label for="validationCustom03">年份 -YY</label>
              <input type="text" class="form-control" id="validationCustom03" placeholder="2018" required>
            </div>
            <div class="col-md-3 mb-3">
              <label for="validationCustom04">月份 -MM</label>
              <input type="text" class="form-control" id="monthCampaign" placeholder="08" required>
            </div>
            <div class="col-md-6 mb-3">
              <label for="validationCustom05">活動名稱</label>
              <input type="text" class="form-control" id="nameCampaign" placeholder="summer-sale" required>
            </div>
            <div>
              常用標籤：
              <button type="button" class="btn btn-primary btn-sm">夏日特賣</button>
              <button type="button" class="btn btn-primary btn-sm">團購檔期</button>
              <label>ＯＯＯＯＯ</label>
            </div>
          </div>

          
            <div class="form-group">
            <hr>
            <h5>最終網址</h5>
                        <!--Final link tag-->
            <textarea class="form-control" id="finalLink" rows="1">

              
            </textarea>
            <p id="finalLink">  </p>
            <p style="font-size=9px">複製上面的網址，之後再去縮網址！</p> 
            </div>
        </form>
        
        <button class="btn btn-primary" onclick="btngeturl()">產生網址</button>
        <button class="btn btn-primary" onclick="erase()">消除資料   </button>
        <button type="button" class="btn btn-info">遞交給Spreadsheet</button>
  </div>
  </div>
  </div>
</div>  <!---contianer-->