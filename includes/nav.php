<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-light"
id="mainNav">
  <a class="navbar-brand" href="index.php" id="logo">艸木內部系統</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
   <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">

    <ul class="navbar-nav .ml-md-auto">
      <li class="nav-item">
        <a class="nav-link" href="https://www.homiya.co">官網 <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://www.homiya.com.tw">部落格 <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://facebook.com/planter.design">粉絲專頁</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          電商工具
        </a>
        
        <!--下拉式選單-->
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="utmbuilder.php">UTM Builder</a>
          <a class="dropdown-item" href="GAsheet.html">Sheet from GA</a>
          <a class="dropdown-item" href="https://business.facebook.com/select/?next=https%3A%2F%2Fbusiness.facebook.com%2F">Facebook business</a>
          <a class="dropdown-item" href="https://analytics.google.com/analytics/web/#report/defaultid/a83018011w122645032p128328135/">Google analytics</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="https://admin.shoplineapp.com/admin/homiya/"></a>
          <a class="dropdown-item" href="#">Line@</a>
          <a class="dropdown-item" href="#">粉專私訊</a>
        </div>

      </li>
      <li class="nav-item">
        <a class="nav-link" href="asset/note.html">開發筆記</a>
      </li>
    </ul>
    <!--搜尋框-->
    <form class="form-inline my-2 my-lg-0 ml-auto">
      <input class="form-control mr-sm-2" type="search" placeholder="暫無功能" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">搜尋</button>
    </form>
    </div>
</nav>

<div class="container">
</div>
