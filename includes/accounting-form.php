<section id="form">
    <div class="wrapper">

    <div class="card">
    <iframe name="hidden_iframe" style="display:none;"></iframe>
    <form action="https://script.google.com/macros/s/AKfycbzfb5ym7b7dRoOiYe8BbU2WzGi1OEedjGqZN0dk8N4yNCXkOAs/exec" method="post" target="hidden_iframe" id="accountFomr">
    <input type="hidden" name="method" value="write" />
  <h1 style="text-align: center;">報帳系統</h1><hr>

<!--date-->
<div class="input-group">
    <div>發票日期：
    <input class="input--style-1 js-datepicker" type="date" placeholder="發票日期" name="date">
     <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i><br>
     <label>日期格式：</label></div>
</div>

<!--cost-->
<div class="input-group">
    <select type="text" name="cost" placeholder="收入/支出" title="收入/支出">
    <option value="收入">收入</option>
    <option value="支出">支出</option>
    </select><br/>
</div>

<!--subject-->
<div class="input-group">
    <select type="text" name="subject" id="" title="subject">
    <label>主科目</label>
    <option value="銷售收入">銷售收入</option>
    <option value="材料費">材料費</option>
    <option value="印刷費">印刷費</option>
    <option value="行銷費">行銷費</option>
    <option value="人事費">人事費</option>
    <option value="經常性支出">經常性支出</option>
    </select>
</div>

<!--subjectB-->
<div class="input-group">
    <select type="text" name="subjectB" id="" title="subjectB" title="subjectB">
    <option value="銷貨成本">材料費-銷貨成本</option>
    <option value="包裝材料">材料費-包裝材料</option>
    <option value="物流費用">黑貓、大榮etc</option>
    <option value="郵局包裹">郵局包裹</option>
    <option value="差旅費">經常性支出-差旅費</option>
    <option value="雜支">經常性支出-雜支</option>
    <option value="郵資">經常性支出-掛號費</option>
    </select>
</div>

<!--amount-->
<div class="input-group">
    <input type="text" name="amount" placeholder="金額" title="amount" /><br/>
</div>


<!--發票號碼 invoiceNum-->
<div class="input-group">
    <input type="text" name="invoiceNum" placeholder="發票號碼" title="invoiceNum"/><br/>
</div>

<!--物品名 item-->
<div class="input-group">
    <input type="text" name="item" placeholder="實際物品名稱" title="item"/><br/>
    <label>例如：7-11運費</label>
</div>



<!--請款人 claim-->
<div class="input-group">
    <select type="text" name="claim" id="">
        <option value="Randy">Randy</option>
        <option value="Wei">Wei</option>
        <option value="White">White</option>
        <option value="Yu-NI">Yu-NI</option>
    </select>
    <label>誰付這筆錢的？</label>
</div>

  <input type="submit" value="提交資料"/>
  <input type="reset" value="清除資料">
</form>

    </div>
    </div>
</section>